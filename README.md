# Ans1Vag 
Create a simple control and webserver networked to get Ansible bootstrapped.

## How to set up
* Install Virtualbox .
* Install Vagrant.
* create a keys folder.
* Generate ansible ssh keys in repo Keys folder.
```
mkdir Keys&&cd Keys
ssh-keygen -t ed25519 -f Keys/ansible_key
```
* run vagrant
```
vagrant up&&vagrant ssh control
```
If you get a pong it's working.
