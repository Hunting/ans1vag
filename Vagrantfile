# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
CONTROL_IP="192.168.0.4"
DB_IP="192.168.0.5"
WEB_IP="192.168.0.6"
DOCK8S_IP="192.168.0.7"
$pubkey=<<-'SCRIPT'
cat /vagrant/Keys/ansible_key.pub>>/home/vagrant/.ssh/authorized_keys
SCRIPT

Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.
  
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = true
  config.vm.boot_timeout = 600
  config.vm.communicator = "ssh"
  config.ssh.username="vagrant"
  config.ssh.insert_key=false

  config.vm.define "db" do |db|
    db.vm.box = "centos/7"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: DB_IP
    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    db.vm.provider "virtualbox" do |vb|
      #   # Display the VirtualBox GUI when booting the machine
      #   vb.gui = true
      vb.cpus=1 
      #   # Customize the amount of memory on the VM:
      vb.memory = "1024"
      vb.customize ["modifyvm", :id, "--vram", "7"]
      vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
      vb.name = "db"
    end
  db.vm.network "forwarded_port",id: "ssh", guest: 22, host: 22225
  db.vm.network "forwarded_port",id: "psql", guest: 5432, host: 54320
  db.ssh.guest_port=22225
  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  db.vm.provision "shell",inline:$pubkey
  db.vm.provision "shell", inline: <<-'SHELL'
    sed -i "$ a #{CONTROL_IP} control\n#{WEB_IP} web\n#{DOCK8S_IP} dock8s" /etc/hosts
  SHELL
  end

  config.vm.define "web" do |web|
    web.vm.box="centos/7"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: WEB_IP
    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    web.vm.provider "virtualbox" do |vb|
      #   # Display the VirtualBox GUI when booting the machine
      #   vb.gui = true
      vb.cpus=1 
      #   # Customize the amount of memory on the VM:
      vb.memory = "1024"
      vb.customize ["modifyvm", :id, "--vram", "7"]
      vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
      vb.name = "web"
    end
  web.vm.network "forwarded_port",id: "ssh", guest: 22, host: 22226
  web.vm.network "forwarded_port",id: "tomcat", guest: 8080, host: 9090
  web.ssh.guest_port=22226
  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  web.vm.provision "shell",inline:$pubkey
  web.vm.provision "shell", inline: <<-'SHELL'
    sed -i "$ a #{CONTROL_I}P control\n#{DB_IP} db\n#{DOCK8S_IP} dock8s" /etc/hosts
  SHELL
  end

  config.vm.define "dock8s" do |dock8s|
    dock8s.vm.box="hashicorp/bionic64"
    dock8s.vm.hostname = "dock8s"
    dock8s.vm.network "private_network", ip: DOCK8S_IP
    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    dock8s.vm.provider "virtualbox" do |vb|
      #   # Display the VirtualBox GUI when booting the machine
      #   vb.gui = true
      vb.cpus=1 
      #   # Customize the amount of memory on the VM:
      vb.memory = "4096"
      vb.customize ["modifyvm", :id, "--vram", "7"]
      vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.name = "dock8s"
    end
  dock8s.vm.synced_folder ".", "/vagrant", mount_options: ["fmode=755", "dmode=774"]
  dock8s.vm.network "forwarded_port",id: "ssh", guest: 22, host: 22227
  dock8s.vm.network "forwarded_port",id: "tomcat", guest: 8080, host: 10010
  dock8s.ssh.guest_port=22227
  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  dock8s.vm.provision "shell",inline:$pubkey
  dock8s.vm.provision "shell", path:"VagScripts\\dock8set.sh"
  end

  config.vm.define "control" do |control|
    control.vm.box = "hashicorp/bionic64"
    control.vm.hostname = "control"
    control.vm.network "private_network", ip: CONTROL_IP
    # Provider-specific configuration so you can fine-tune various
    # backing providers for Vagrant. These expose provider-specific options.
    # Example for VirtualBox:
    #
    control.vm.provider "virtualbox" do |vb|
      #   # Display the VirtualBox GUI when booting the machine
      #   vb.gui = true
      vb.cpus=1 
      #   # Customize the amount of memory on the VM:
      vb.memory = "1024"
      vb.customize ["modifyvm", :id, "--vram", "7"]
      vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
      vb.name = "control"
    end
  control.vm.network "forwarded_port",id: "ssh", guest: 22, host: 22224
  control.ssh.guest_port=22224
  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  control.vm.provision "file", source: "Keys\\ansible_key", destination: "/home/vagrant/.ssh/ansible_key"
  control.vm.provision "shell", path:"VagScripts\\conset.sh"
  end
end
