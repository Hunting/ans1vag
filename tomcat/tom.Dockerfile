FROM centos:7
WORKDIR /vagrant
RUN yum -y update&&\
yum -y install java-11-openjdk-devel&&\
yum -y install tomcat&&\
rpm -e --nodeps java-1.8.0-openjdk java-1.8.0-openjdk-headless&&\
yum clean all&&\
rm -rf /var/cache/yum&&\
ln -s /var/lib/tomcat/webapps/td /var/lib/tomcat/webapps/ROOT
COPY td.war /var/lib/tomcat/webapps/
EXPOSE 8080/tcp
CMD ["/usr/libexec/tomcat/server","start"]
