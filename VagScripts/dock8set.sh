#!/bin/sh
CONTROL_IP=192.168.0.4
DB_IP=192.168.0.5
WEB_IP=192.168.0.6

sed -i "$ a $CONTROL_IP control\n$DB_IP db\n$WEB_IP web" /etc/hosts

sed -e \
"$ a ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBDZdaZqXgkjDcWzkoMLUyi8e1CaQX62Wa7BYd8tPGfY putty"\
 -e "$ a ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE1Jc8W4BhxB1SuN16eKAxptYZ/nhk5FrCcwIDIrGa0m 301506"\
 -i /home/vagrant/.ssh/authorized_keys

apt-get -y update
apt-get -y install docker.io unzip
snap install kubectl --classic

curl -sL "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose\
&&chmod +x /usr/local/bin/docker-compose

curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.8.0/kind-$(uname)-amd6\
&&chmod +x ./kind&&mv ./kind /usr/local/bin

curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator\
&&curl -o aws-iam-authenticator.sha256 https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator.sha256\
&&openssl sha1 -sha256 aws-iam-authenticator&&rm aws-iam-authenticator.sha256\
&&chmod +x ./aws-iam-authenticator&&mv ./aws-iam-authenticator /usr/local/bin

curl -L "https://github.com/jenkins-x/jx/releases/download/$(curl --silent "https://github.com/jenkins-x/jx/releases/latest" | sed 's#.*tag/\(.*\)\".*#\1#')/jx-linux-amd64.tar.gz" | tar xzv "jx"\
&&mv jx /usr/local/bin

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"\
&&unzip awscliv2.zip&&./aws/install -b /usr/local/bin -i /usr/local/aws-cli\
&&rm -r awscliv2.zip ./aws

curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz"\
|tar xz -C /tmp&&mv /tmp/eksctl /usr/local/bin

wget https://releases.hashicorp.com/terraform/0.12.26/terraform_0.12.26_linux_amd64.zip\
&&unzip terraform_0.12.26_linux_amd64.zip&&mv terraform /usr/local/bin&&rm terraform_0.12.26_linux_amd64.zip

wget https://releases.hashicorp.com/vault/1.4.2/vault_1.4.2_linux_amd64.zip\
&&unzip vault_1.4.2_linux_amd64.zip -d /usr/local/bin&&rm vault_1.4.2_linux_amd64.zip

sed -i "$ a set number tabstop=2 shiftwidth=2 softtabstop=2 noexpandtab" /etc/vim/vimrc

systemctl enable docker
systemctl start docker

usermod -aG docker vagrant
runuser -l vagrant -c \
'docker-compose -f /vagrant/docker-compose.yml up -d --build'
