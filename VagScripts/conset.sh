#!/bin/sh
DB_IP=192.168.0.5
WEB_IP=192.168.0.6
DOCK8S_IP=192.168.0.7
ANSIBLE_USER=vagrant
ANSIBLE_SSH_KEY=/home/vagrant/.ssh/ansible_key
chmod 600 /home/vagrant/.ssh/ansible_key

sed -i "$ a $DB_IP db\n$WEB_IP web\n$DOCK8S_IP dock8s" /etc/hosts

apt-get update
apt-get -y install software-properties-common
apt-add-repository --yes --update ppa:ansible/ansible
apt-get -y install ansible

cat /vagrant/playbooks/inventory/hosts>>/etc/ansible/hosts

sed -e "/deprecation_warnings/ s/^#//"\
 -e "/deprecation_warnings/ s/True/False/" -i /etc/ansible/ansible.cfg

sed -i "$ a ssh-keyscan -Ht ed25519 web db docker>/home/$ANSIBLE_USER/.ssh/known_hosts\
\nansible -m ping all\n\
ansible-playbook /vagrant/playbooks/tompgsql.yml" /home/$ANSIBLE_USER/.bashrc
