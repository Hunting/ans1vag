FROM centos:7
WORKDIR /vagrant
RUN yum -y update&&\
yum -y install postgresql-server&&\
yum clean all&&\
rm -rf /var/cache/yum
EXPOSE 5432/tcp
USER postgres
RUN /usr/bin/initdb /var/lib/pgsql/data&&\
echo "host all all sz-td.webstack trust">>/var/lib/pgsql/data/pg_hba.conf&&\
sed -e "/#listen_addresses/ s/localhost/*/" -e "/#listen_addresses/ s/^#//"\
 -i /var/lib/pgsql/data/postgresql.conf
CMD ["/usr/bin/postgres","-D","/var/lib/pgsql/data","-s","-p 5432"]
