<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.TimeZone" %>
<html>
<head>
<title>Hello!</title>
</head>
<body>
<center>
<h1>Hello!</h1>
<h1>Current Date & Time</h1>
</center>
<%Date today = new Date();
DateFormat df = new SimpleDateFormat("EEEE, d MMMM yyyy HH:mm:ss");
df.setTimeZone(TimeZone.getTimeZone("Europe/London"));
String GMT = df.format(today);
out.print( "<h2 align = \"center\">" +GMT.toString()+"</h2>");
%>
</body>
</html>
